require 'httparty'
require 'pry'
require 'benchmark'
require 'json'

require 'sequel'

DB = Sequel.connect('postgres://api_performance:api_performance@localhost/api_performance')

class KeysetRunner
  attr_reader :endpoint

  def initialize(endpoint)
    @endpoint = endpoint
  end

  def execute(max_page = 100)
    id_after = nil
    response = nil
    (1..max_page).each do |page|
      url = "#{endpoint}&per_page=20&id_after=#{id_after}"

      realtime = Benchmark.realtime do
        response = HTTParty.get(url)
      end

      yield url, page, realtime

      puts "#{response.code} : #{response.message}" unless response.code == 200

      id_after = last_project_id(response.body)
    end
  end

  private

  def last_project_id(body)
    data = JSON.parse(body)

    data.last['id']
  end
end

class OffsetRunner
  attr_reader :endpoint

  def initialize(endpoint)
    @endpoint = endpoint
  end

  def execute(max_page = 100)
    response = nil
    (1..max_page).each do |page|
      url = "#{endpoint}&per_page=20&page=#{page}"

      realtime = Benchmark.realtime do
        response = HTTParty.get(url)
      end

      yield url, page, realtime

      puts "#{response.code} : #{response.message}" unless response.code == 200
    end
  end
end

url = 'https://gitlab.com/api/v4/projects?order_by=id&sort=asc'

[OffsetRunner, KeysetRunner].each do |runner_type|
  runner_type.new(url).execute(10000) do |url, page, realtime|
    record = {
      runner: runner_type.to_s,
      url: url,
      page: page,
      timing: realtime
    }
    DB[:pagination].insert(record)

    puts record
  end
end
