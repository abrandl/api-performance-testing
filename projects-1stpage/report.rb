require 'sequel'
require 'pry'
require 'csv'
require 'json'

DB = Sequel.connect('postgres://api_performance:api_performance@localhost/api_performance')

def transform(r)
  params = JSON.parse(r[:params])
  params.delete('order_by')
  params.delete('sort')

  return nil if params.size > 1

  r[:params] = params.to_a.sort.map { |e| e.join('=') }.join('&')
  sorting = JSON.parse(r[:sorting])

  r[:sorting] = sorting['order_by'] + ' ' + sorting['sort']
  r
end

result = DB.fetch(<<~SQL).map { |r| transform(r) }.compact
  select
    path, params, sorting,
    count(*) as samples,
    avg(timing) as timing_avg,
    percentile_disc(0.5) within group (order by timing) as timing_p50,
    percentile_disc(0.95) within group (order by timing) as timing_p95,
    percentile_disc(0.99) within group (order by timing) as timing_p99,
    max(timing) as timing_max,
    stddev(timing) as timing_stddev
  from calls
  where status = 200 and url ~* 'private_token'
  group by 1, 2, 3
SQL

keys = result.first.keys

CSV.open('report_authenticated.csv', 'wb+') do |csv|
  csv << keys

  result.each do |row|
    csv << keys.map { |k| row[k] }
  end
end
