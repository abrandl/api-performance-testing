require 'httparty'
require 'pry'
require 'benchmark'
require 'json'

require 'sequel'

DB = Sequel.connect('postgres://api_performance:api_performance@localhost/api_performance')

Configuration = Struct.new(:url, :order, :params)

class APICall
  attr_reader :endpoint, :order, :params
  def initialize(endpoint, order, params)
    @endpoint = endpoint
    @order = order
    @params = params
  end

  def url
    endpoint.url + '?' + all_params.to_a.map { |e| e.join('=') }.join("&")
  end

  def all_params
    order.merge(params || {})
  end

  def execute
    response = HTTParty.get(url)

    return response.code
  end
end

class APIEndpoint
  attr_reader :url

  def initialize(url)
    @url = url
    @params = []
    @orders = []
    @sorts = []
  end

  def orders(columns)
    columns.each do |column|
      order(column)
    end
    self
  end

  def order(column)
    @orders << {order_by: column}
    self
  end

  def sorts(directions)
    directions.each do |dir|
      sort(dir)
    end
    self
  end

  def sort(dir)
    @sorts << {sort: dir}
    self
  end

  def params(hash)
    hash.each do |k, v|
      [v].flatten.each do |value|
        @params << {k => value}
      end
    end
    self
  end

  def api_calls
    orderings = merge(@orders.product(@sorts))
    params = merge(combinations(@params)).uniq

    orderings.product(params).map do |o, p|
      APICall.new(self, o, p)
    end
  end

  private

  def merge(params)
    params.map { |o| o.reduce { |h, e| h.merge(e) } }
  end

  def combinations(arr)
    arr.product(arr)
  end

  def powerset(arr)
    a = [[]]
    for i in 0...arr.size do
      len = a.size; j = 0;
      while j < len
        a << (a[j] + [arr[i]])
        j+=1
      end
    end
    a
  end
end

api = APIEndpoint.new('https://gitlab.com/api/v4/projects')

api
  .orders(%i[id name path created_at updated_at last_activity_at])
  .sorts(%i[asc desc])
  .params(archived: [true, false])
  .params(visibility: %w[public internal private])
  .params(search: 'gitlab')
  .params(simple: [true, false])
  .params(owned: [true, false])
  .params(membership: [true, false])
  .params(starred: [true, false])
  .params(statistics: [true, false])
  .params(with_custom_attributes: [true, false])
  .params(with_issues_enabled: [true, false])
  .params(with_merge_requests_enabled: [true, false])
  .params(with_programming_language: 'ruby')
  .params(wiki_checksum_failed: [true, false])
  .params(repository_checksum_failed: [true, false])
  .params(min_access_level: [10,20,30,40,50])

calls = api.api_calls

5.times do
  calls.shuffle.each do |call|
    record = {
      url: call.url,
      path: URI.parse(call.url).path,
      params: call.all_params.to_json,
      sorting: call.order.to_json
    }

    if DB[:calls].where(record).count >= 5
      puts "#{Time.now}\t(skipped)\t#{call.url}"
      next
    end

    status = nil
    time = Benchmark.realtime do
      begin
        status = call.execute
      rescue => e
        puts "#{Time.now}\t(error)\t#{call.url}\t#{e}"
        next
      end
    end

    puts "#{Time.now}\t#{time.round(2)}\t#{call.url}"

    record.merge!(status: status, timing: time)

    DB[:calls].insert(record)
  end
end