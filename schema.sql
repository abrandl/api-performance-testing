--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: calls; Type: TABLE; Schema: public; Owner: abrandl-gl
--

CREATE TABLE public.calls (
    id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    url character varying NOT NULL,
    path character varying NOT NULL,
    params jsonb NOT NULL,
    status integer NOT NULL,
    timing numeric,
    sorting jsonb
);


ALTER TABLE public.calls OWNER TO "abrandl-gl";

--
-- Name: calls_id_seq; Type: SEQUENCE; Schema: public; Owner: abrandl-gl
--

CREATE SEQUENCE public.calls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calls_id_seq OWNER TO "abrandl-gl";

--
-- Name: calls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abrandl-gl
--

ALTER SEQUENCE public.calls_id_seq OWNED BY public.calls.id;


--
-- Name: pagination; Type: TABLE; Schema: public; Owner: abrandl-gl
--

CREATE TABLE public.pagination (
    id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    url character varying,
    page integer,
    timing numeric,
    runner character varying
);


ALTER TABLE public.pagination OWNER TO "abrandl-gl";

--
-- Name: pagination_id_seq; Type: SEQUENCE; Schema: public; Owner: abrandl-gl
--

CREATE SEQUENCE public.pagination_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pagination_id_seq OWNER TO "abrandl-gl";

--
-- Name: pagination_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abrandl-gl
--

ALTER SEQUENCE public.pagination_id_seq OWNED BY public.pagination.id;


--
-- Name: calls id; Type: DEFAULT; Schema: public; Owner: abrandl-gl
--

ALTER TABLE ONLY public.calls ALTER COLUMN id SET DEFAULT nextval('public.calls_id_seq'::regclass);


--
-- Name: pagination id; Type: DEFAULT; Schema: public; Owner: abrandl-gl
--

ALTER TABLE ONLY public.pagination ALTER COLUMN id SET DEFAULT nextval('public.pagination_id_seq'::regclass);


--
-- Name: calls calls_pkey; Type: CONSTRAINT; Schema: public; Owner: abrandl-gl
--

ALTER TABLE ONLY public.calls
    ADD CONSTRAINT calls_pkey PRIMARY KEY (id);


--
-- Name: pagination pagination_pkey; Type: CONSTRAINT; Schema: public; Owner: abrandl-gl
--

ALTER TABLE ONLY public.pagination
    ADD CONSTRAINT pagination_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

